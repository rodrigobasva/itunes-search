import {Component, OnDestroy, OnInit} from "@angular/core";
import {SearchService} from "../search.service";
import {Subject} from "rxjs";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";

@Component({
  templateUrl: "./artist-search.component.html",
  styleUrls: ["./artist-search.component.css"]
})
export class ArtistSearchComponent implements OnInit, OnDestroy {
  
  artists: any[] = [];
  debounce: Subject<string> = new Subject<string>();

  constructor(private searchService: SearchService) {}
  
  ngOnInit(): void {
    this.debounce
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(term => this.searchArtist(term));
  }

  ngOnDestroy(): void {
    this.debounce.unsubscribe();
  }

  searchArtist(term: string) {
    this.searchService.getArtists(term)
      .subscribe(artists => {
        this.artists = artists.results;
        console.log(this.artists);
      });
  }
}
