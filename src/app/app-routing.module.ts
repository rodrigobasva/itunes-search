import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from "./home/home.component";
import { TrackSearchComponent } from "./track-search/track-search.component";
import { ArtistSearchComponent } from "./artist-search/artist-search.component";
import { AboutComponent } from "./about/about.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'track-search', component: TrackSearchComponent},
  {path: 'artist-search', component: ArtistSearchComponent},
  {path: 'about', component: AboutComponent},
  {path: '**', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
