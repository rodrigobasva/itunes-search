import {Component, EventEmitter, OnDestroy, OnInit} from "@angular/core";
import {SearchService} from "../search.service";
import {Subject} from "rxjs";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";

@Component({
  templateUrl: './track-search.component.html',
  styleUrls: ['./track-search.component.css']
})
export class TrackSearchComponent implements OnInit, OnDestroy {

  tracks: any[] = [];
  debounce: Subject<string> = new Subject<string>();

  constructor(private searchService: SearchService) {}
  
  ngOnInit(): void {
    this.debounce
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(term => this.searchTrack(term));
  }

  ngOnDestroy() {
    this.debounce.unsubscribe();
  }

  searchTrack(term: string) {
    this.searchService.getTracks(term)
      .subscribe(tracks => {
        this.tracks = tracks.results;
        console.log(this.tracks);
      });
  }
}
