export class Track {
  artworkUrl60: string;
  trackCensoredName: string;
  artistName: string;
}
