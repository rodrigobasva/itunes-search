import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  API = "https://itunes.apple.com/";
  
  constructor(private http: HttpClient) { }

  getTracks(term: string): Observable<any> {
    return this.http.get(this.API + "/search?term=" + term + "&entity=musicTrack");
  }

  getArtists(term: string): Observable<any> {
    return this.http.get(this.API + "/search?term=" + term + "&entity=musicArtist");
  }
  
  getInfoArtist(artistId: string): Observable<any> {
    return this.http.get(this.API + "/lookup?id=" + artistId);
  }
}
