import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'miliTime'
})
export class MiliTimePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let milliseconds = parseInt(value);
    let hours = Math.floor(milliseconds / 3600000);
    let minutes = Math.floor((milliseconds - (hours * 3600000)) / 60000);
    let seconds = Math.floor((milliseconds - (hours * 3600000) - (minutes * 60000)) / 1000);
    let secondsStr = seconds.toString().padStart(2, '0');
    return minutes + "'" + " " + secondsStr + "''";
  }
}
