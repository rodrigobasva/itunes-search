import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from "@angular/common/http";
import { CommonModule} from "@angular/common";
import { FormsModule} from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchService} from "./search.service";
import { MiliTimePipe } from './mili-time.pipe';
import { HomeComponent } from "./home/home.component";
import { TrackSearchComponent } from "./track-search/track-search.component";
import { TopnavComponent } from "./topnav/topnav.component";
import { ArtistSearchComponent } from "./artist-search/artist-search.component";
import { AboutComponent } from "./about/about.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TopnavComponent,
    TrackSearchComponent,
    ArtistSearchComponent,
    AboutComponent,
    MiliTimePipe
  ],
  imports: [
    FormsModule,
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  exports: [
    CommonModule
  ],
  providers: [SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
